import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import logo from '../assets/adrenalin.svg';


const MainMenu = () => {
    return (
        <nav class="navbar navbar-expand-md navbar-light bg-light">
            <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                <img src={logo} alt="Logo" className="logo"/>
            </div>

            <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="navItem" href="#">Culture</a>
                    </li>
                    <li class="nav-item">
                        <a class="navItem" href="#">Work</a>
                    </li>
                    <li class="nav-item">
                        <a class="navItem" href="#">Clients</a>
                    </li>
                    <li class="nav-item">
                        <a class="navItem" href="#">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="navItem" href="#">Careers</a>
                    </li>
                    <li class="nav-item">
                        <a class="navItem" href="#">Contact</a>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

export default MainMenu;