import React from 'react';
import BootstrapTable from "react-bootstrap-table-next";
import data from '../feed/data.json';
import Image from 'react-bootstrap/Image';
import { Link } from 'react-router-dom'
import { Container, Row, Col } from 'reactstrap';

const CardPlacer = () => {
    return (
        <div className="mainContent">
            <Container fluid className="container-list">
                {
                    data.map((postDetail, index) => {
                        return (
                            
                            <div className="col-md-6 col-sm-6" key={postDetail.id}>
                                <Image src={require(`../assets/${postDetail.thumb}`)} alt={postDetail.tag}/>
                                
                                <div className="prodDescription">{postDetail.title_long}</div>
                                <div className="caseStudyLink"> 

                                        <Link 
                                            key={postDetail.id}
                                            to={'/prodDetail/' + postDetail.id}
                                        >
                                            -- VIEW CASE STUDY
                                        </Link>
    
                                </div>
                            </div>
                        )
                    })
                }
            </Container>
        </div>
    );

}

export default CardPlacer