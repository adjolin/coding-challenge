import React, { Component} from 'react';
import data from '../feed/data.json';
import { Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom'

function getDataFromArray(array, id) {
    return array.find((element) => {
        return element.id.toString() === id.toString();
    })
}


class CardDetail extends Component {
    constructor(props) { 
        super(props);
        this.routeParam = props.match.params.id;

    }

    

    
    render() {
        let cardData = getDataFromArray(data, [this.routeParam]);
        return (
            <div className="mainContent">
            <Container fluid>
                <Row>
                    <Col>
                            <Link
                                to={'/'}
                            >
                                Back
                            </Link>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <img src={require(`../assets/${cardData.image}`)} alt={data.tag} />
                    </Col>
                    <Col>
                        <div className="prodTitle">{cardData.title}</div>
                        <div className="questions">
                            {
                                cardData.questions.map((questions, index) => {
                                    return <div><div className="questionTitle"> Question {index + 1} </div>  <div className="questionText"> {questions}</div></div>
                                })
                            }
                        </div>
                    </Col>
                </Row>
            </Container>
            </div>
        );
    }
}

export default CardDetail

/**
 * 
 * <div className="col-md-6 col-sm-6">
                    <img src={require(`../assets/${cardData.image}`)} alt={data.tag}  />
                </div>
                <div className="col-md-6 col-sm-6">
                    <div className="prodTitle">{cardData.title}</div>
                    <div className="questions">
                        {
                            cardData.questions.map((questions, index) => {
                                return <div><div> Question {index} </div>  <div> {questions}</div></div>
                            })
                        }
                    </div>
                </div>
 */