import React from 'react';

import logo from '../assets/adrenalin.svg';


const Footer = () => {
    return (
        <nav class="navbar navbar-expand-md navbar-light bg-light">
            <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                <img src={logo} alt="Logo" className="logo"/>
            </div>

            <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="navItem" href="#">Privacy</a>
                    </li>
                    <li class="nav-item">
                        <a class="navItem" href="#">Sitemap</a>
                    </li>
                    <li class="nav-item">
                        <a class="navItem" href="#">Facebook</a>
                    </li>
                    <li class="nav-item">
                        <a class="navItem" href="#">Linkedin</a>
                    </li>
                    <li class="nav-item">
                        <a class="navItem" href="#">Instagram</a>
                    </li>
                    <li class="nav-item">
                        <a class="navItem" href="#">Twitter</a>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

export default Footer;