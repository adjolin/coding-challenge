// MyComponent.test.js
import React from 'react';
import { shallow } from 'enzyme';
import CardPlacer from './components/CardPlacer';
describe("CardPlacer", () => {
  it("should render my component", () => {
    expect(shallow(<CardPlacer />).find('.mainContent').exists()).toBe(true)
  });
});