import React, { Component } from 'react';
import CardContainer from './components/CardContainer'
import MainMenu from './components/MainMenu'
import CardDetail from './components/CardDetail'
import { Row, Container } from 'react-bootstrap';
import './assets/css/custom.css'
import Footer from './components/Footer';
import ColoredLine from './components/ColoredLine';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom'

class App extends Component {
  
  render() {
    return (
      <Container fluid>  
        <MainMenu />
        <Router>
          <Switch>
            <Route path='/' exact component={CardContainer} />
            
            <Route path='/prodDetail/:id' component={CardDetail} />
          </Switch>
        </Router>  
        <ColoredLine color="black" />
        <Footer />
      </Container>
    );
  }
}
export default App;